FACE DETECTION IN ROS ENVIRONMENT  

Project description:
             Its a program for detecing human faces in ROS environment using opencv. Here i’m using haarcascade algorithm  for detecting the face, which is available in opencv. You need to  install ros in your Linux os ( i’m using ubuntu 16.04) by using this link:

http://wiki.ros.org/kinetic/Installation/Ubuntu

Requirements:

1. Update packages

Run the following command to update the package list and upgrade all of your system software to the latest version available

sudo apt-get update && sudo apt-get upgrade

2. cv_bridge

For giving the images in opencv to ROS environment we need cv bridge. For that we need to install cv-brigde package.

sudo apt-get install python-cv-bridge

3. OpenCv

sudo apt-get install python-opencv


After installing all the above things follow these steps:    

1.Create a catkin workspace
 Follow the link: http://wiki.ros.org/catkin/Tutorials/create_a_workspace

2.Create a catkin Package
 Follow the link: http://wiki.ros.org/ROS/Tutorials/CreatingPackage

